package com.visma.wms.locations;

public enum LocationType {

    STORAGE,
    PACKAGING,
    SHIPPING

}
