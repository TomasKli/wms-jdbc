package com.visma.wms.locations;


import com.visma.wms.inventory.Item;

import java.util.List;

public class Location {

    private String name;
    private List<Item> items;
    private LocationType locationType;

    public Location(String name, LocationType locationType) {
        this.name = name;
        this.locationType = locationType;
    }

    public void addItem(Item item){
        items.add(item);
    }

    public List<Item> getItems() {
        return items;
    }

    public String getName() {
        return name;
    }

    public LocationType getLocationType() {
        return locationType;
    }
}
