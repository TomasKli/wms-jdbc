package com.visma.wms.inventory;

import java.util.Date;

public class Item {

    String label;
    Date dateCreated;
    Date dateShipped;
    String locationName;

    public Item(String label) {
        this.label = label;
        this.dateCreated = new Date();
    }

    public Item(String label, String locationName, Date dateCreated, Date dateShipped) {
        this.label = label;
        this.locationName = locationName;
        this.dateCreated = dateCreated;
        this.dateShipped = dateShipped;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public Date getDateShipped() {
        return dateShipped;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setDateShipped(Date dateShipped) {
        this.dateShipped = dateShipped;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (label != null ? !label.equals(item.label) : item.label != null) return false;
        if (dateCreated != null ? !dateCreated.equals(item.dateCreated) : item.dateCreated != null) return false;
        if (dateShipped != null ? !dateShipped.equals(item.dateShipped) : item.dateShipped != null) return false;
        return locationName != null ? locationName.equals(item.locationName) : item.locationName == null;

    }

    @Override
    public int hashCode() {
        int result = label != null ? label.hashCode() : 0;
        result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
        result = 31 * result + (dateShipped != null ? dateShipped.hashCode() : 0);
        result = 31 * result + (locationName != null ? locationName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Item{" +
                "label='" + label + '\'' +
                ", dateCreated=" + dateCreated +
                ", dateShipped=" + dateShipped +
                ", locationName='" + locationName + '\'' +
                '}';
    }
}
