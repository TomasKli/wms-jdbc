package com.visma.wms.application_test;

import com.visma.wms.data_access.InMemoryPersistence;
import com.visma.wms.data_access.jdbc.DatabasePersistence;
import com.visma.wms.service.WMSService;

public class WMSTestRun {

    private static final String HAT = "Hat";
    private static final String SHIRT = "Shirt";
    private static final String PANTS = "Pants";
    private static final String BOOTS = "Boots";
    String[] items = new String[]{HAT, SHIRT, PANTS, BOOTS};

    public static void main(String[] args) {
//        WMSService service = new WMSService(new InMemoryPersistence());
        WMSService service = new WMSService(new DatabasePersistence());
        WMSTestRun testRun = new WMSTestRun(service);
        testRun.executeTest();
    }

    private WMSService service;

    public WMSTestRun(WMSService service) {
        this.service = service;
    }

    public void executeTest(){
        importItems();
        printAllItems();

        moveToPackaging(HAT);
        moveToPackaging(PANTS);
        moveToPackaging(BOOTS);

        moveToShipping(PANTS);
        moveToShipping(BOOTS);

        shipItem(BOOTS);

        service.emptyWarehouse();
    }

    private void moveToPackaging(String itemLabel) {
        System.out.print("Moving " + itemLabel + " to packaging - ");
        service.moveItemToPackagingLocation(itemLabel);
        printItem(itemLabel);
    }
    private void moveToShipping(String itemLabel) {
        System.out.print("Moving " + itemLabel + " to shipping - ");
        service.moveToShippingLocation(itemLabel);
        printItem(itemLabel);
    }
    private void shipItem(String itemLabel) {
        System.out.print("Shipping " + itemLabel + " - ");
        service.shipItem(itemLabel);
        printItem(itemLabel);
    }

    private void importItems() {
        for (String item: items){
            service.importItem(item);
        }
    }

    private void printItem(String item) {
        System.out.println(service.getItem(item));
        System.out.println();
    }

    private void printAllItems() {
        for (String item: items){
            System.out.println(service.getItem(item));
        }
        System.out.println();
    }
}
