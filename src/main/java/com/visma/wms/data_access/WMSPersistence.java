package com.visma.wms.data_access;

import com.visma.wms.inventory.Item;
import com.visma.wms.locations.Location;
import com.visma.wms.locations.LocationType;

import java.sql.SQLException;
import java.util.Collection;

public interface WMSPersistence {

    Location getLocation(LocationType locationType);

    void saveItem(Item item);

    Item getItem(String itemLabel);

    void updateItem(Item item);

    void emptyWarehouse();

    void updateLocaiton(Location location);
}
