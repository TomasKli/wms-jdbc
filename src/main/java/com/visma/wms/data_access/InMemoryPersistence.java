package com.visma.wms.data_access;

import com.visma.wms.inventory.Item;
import com.visma.wms.locations.Location;
import com.visma.wms.locations.LocationType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class InMemoryPersistence implements WMSPersistence {

    Map<LocationType, Location> locationsByType = new HashMap<>();
    Map<String, Item> itemsByLabel = new HashMap<>();

    public InMemoryPersistence() {
        locationsByType.put(LocationType.STORAGE, new Location("Storage", LocationType.STORAGE));
        locationsByType.put(LocationType.PACKAGING, new Location("Packaging", LocationType.PACKAGING));
        locationsByType.put(LocationType.SHIPPING, new Location("Shipping", LocationType.SHIPPING));
    }

    public Location getLocation(LocationType locationType) {
        return locationsByType.get(locationType);
    }

    public void saveItem(Item item) {
        itemsByLabel.put(item.getLabel(), item);
    }

    public void updateItem(Item item) {
        itemsByLabel.put(item.getLabel(), item);
    }

    @Override
    public void emptyWarehouse() {
        locationsByType = new HashMap<>();
        itemsByLabel = new HashMap<>();
    }

    @Override
    public void updateLocaiton(Location location) {
        locationsByType.put(location.getLocationType(), location);

    }

    public Item getItem(String itemLabel) {
        return itemsByLabel.get(itemLabel);
    }
}
