package com.visma.wms.data_access.jdbc;

public class DBConstants {

    static final String DB_DRIVER = "org.postgresql.Driver";
    static final String DB_URL = "jdbc:postgresql://localhost:5432/wmsdb";
    static final String DB_USERNAME = "visma";
    static final String DB_PASSWORD = "visma";

}
