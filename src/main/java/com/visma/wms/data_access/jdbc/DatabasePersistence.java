package com.visma.wms.data_access.jdbc;

import com.visma.wms.data_access.WMSPersistence;
import com.visma.wms.inventory.Item;
import com.visma.wms.locations.Location;
import com.visma.wms.locations.LocationType;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import static com.visma.wms.data_access.jdbc.DBConstants.DB_DRIVER;
import static com.visma.wms.data_access.jdbc.DBConstants.DB_PASSWORD;
import static com.visma.wms.data_access.jdbc.DBConstants.DB_URL;
import static com.visma.wms.data_access.jdbc.DBConstants.DB_USERNAME;

public class DatabasePersistence implements WMSPersistence {

    private static final String FIND_LOCATION_BY_TYPE = "SELECT * FROM location where type = ?";
    private static final String FIND_ITEM_BY_LABEL = "SELECT * FROM item where label = ?";
    private static final String SAVE_ITEM = "INSERT INTO item (label, location_name, date_created, date_shipped) VALUES (?, ?, ?, ?)";
    private static final String SAVE_LOCATION = "INSERT INTO location (name, type) VALUES (?, ?)";
    private static final String UPDATE_ITEM = "UPDATE item set label = ?, location_name = ?, date_created = ?, date_shipped = ? WHERE label = ?";
    private static final String DELETE_LOCATIONS = "DELETE FROM item";
    private static final String DELETE_ITEMS = "DELETE FROM location";

    public DatabasePersistence() {
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        saveLocation(new Location("Storage", LocationType.STORAGE));
        saveLocation(new Location("Packaging", LocationType.PACKAGING));
        saveLocation(new Location("SHIPPING", LocationType.SHIPPING));

    }

    private Connection getConnection() {
        try {
            return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    @Override
    public Location getLocation(LocationType locationType) {
        PreparedStatement statement = null;
        try (Connection conn = getConnection()){
            statement = conn.prepareStatement(FIND_LOCATION_BY_TYPE);
            statement.setString(1, locationType.toString());

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                String name = resultSet.getString("name");
                String type = resultSet.getString("type");
                return new Location(name, LocationType.valueOf(type));
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }

    }

    @Override
    public void saveItem(Item item) {
        PreparedStatement statement = null;
        try (Connection conn = getConnection()){
            statement = conn.prepareStatement(SAVE_ITEM);
            statement.setString(1, item.getLabel());
            statement.setString(2, item.getLocationName());
            statement.setDate(3, toSqlDate(item.getDateCreated()));
            statement.setDate(4, toSqlDate(item.getDateShipped()));

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }


    public void saveLocation(Location location) {
        PreparedStatement statement = null;
        try (Connection conn = getConnection()){
            statement = conn.prepareStatement(SAVE_LOCATION);
            statement.setString(1, location.getName());
            statement.setString(2, location.getLocationType().toString());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    @Override
    public Item getItem(String itemLabel) {
        PreparedStatement statement = null;
        try (Connection conn = getConnection()){
            statement = conn.prepareStatement(FIND_ITEM_BY_LABEL);
            statement.setString(1, itemLabel);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                String label = resultSet.getString("label");
                String locationName = resultSet.getString("location_name");
                Date dateCreated = resultSet.getDate("date_created");
                Date dateShipped = resultSet.getDate("date_shipped");
                return new Item(label, locationName, dateCreated, dateShipped);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    @Override
    public void updateItem(Item item) {
        PreparedStatement statement = null;
        try (Connection conn = getConnection()){
            statement = conn.prepareStatement(UPDATE_ITEM);
            statement.setString(1, item.getLabel());
            statement.setString(2, item.getLocationName());
            statement.setDate(3, toSqlDate(item.getDateCreated()));
            statement.setDate(4, toSqlDate(item.getDateShipped()));
            statement.setString(5,item.getLabel());

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    @Override
    public void emptyWarehouse() {
        Statement statement = null;
        try (Connection conn = getConnection()){
            statement = conn.createStatement();
            statement.executeUpdate(DELETE_ITEMS);
            statement.executeUpdate(DELETE_LOCATIONS);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }

    }

    @Override
    public void updateLocaiton(Location location) {
        PreparedStatement statement = null;
        try (Connection conn = getConnection()){
            statement = conn.prepareStatement(FIND_LOCATION_BY_TYPE);
            statement.setString(1, location.getLocationType().toString());

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                resultSet.updateString("name", location.getName());
                resultSet.updateRow();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    private java.sql.Date toSqlDate(Date dateShipped) {
        if (dateShipped == null) {
            return null;
        }
        return new java.sql.Date(dateShipped.getTime());
    }


    private void closeStatement(Statement statement) {
        if(statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }


}
