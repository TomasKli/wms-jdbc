package com.visma.wms.service;

import com.visma.wms.data_access.WMSPersistence;
import com.visma.wms.inventory.Item;
import com.visma.wms.locations.Location;
import com.visma.wms.locations.LocationType;

import java.util.Date;

public class WMSService {

    private WMSPersistence persistence;

    public WMSService(WMSPersistence persistence) {
        this.persistence = persistence;
    }

    public void importItem(String itemLabel) {
        Item item = new Item(itemLabel);
        Location location = persistence.getLocation(LocationType.STORAGE);
        item.setLocationName(location.getName());
        persistence.saveItem(item);
    }

    public void moveItemToPackagingLocation(String itemLabel) {
        Item item = getItem(itemLabel);
        moveItemToLocation(item, LocationType.PACKAGING);
    }

    public  void moveToShippingLocation(String itemLabel) {
        moveItemToLocation(getItem(itemLabel), LocationType.SHIPPING);
    }

    public void shipItem(String itemLabel) {
        Item item = getItem(itemLabel);
        item.setDateShipped(new Date());
        item.setLocationName(null);
        persistence.updateItem(item);
    }

    public Item getItem(String itemLabel) {
        return persistence.getItem(itemLabel);
    }

    public void emptyWarehouse() {
        persistence.emptyWarehouse();
    }

    private void moveItemToLocation(Item item, LocationType locationType) {
        Location location = persistence.getLocation(locationType);
        item.setLocationName(location.getName());
        persistence.updateItem(item);
    }

    public void updateLocation(Location location) {
        persistence.updateLocaiton(location);
    }
}
